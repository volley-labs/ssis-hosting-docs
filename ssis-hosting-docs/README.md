# SSIS Hosting Documentation

SSIS Hosting Documentation

## Installation

```console
npm install
```

## Local Development

```console
npm start
```

This command starts a local development server and open up a browser window. Most changes are reflected live without having to restart the server.

## Build

```console
npm run build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

## Local Test your build

```console
npm run serve
```

This command serves the `build` directory on your local machine.

## Run TypeSense locally

docker volume create typesense-data
docker run --name typesense -d --restart unless-stopped -p 8108:8108 -v typesense-data:/data typesense/typesense:0.23.1 --data-dir /data --api-key=ssis --enable-cors --cors-domains http://localhost,http://host.docker.internal

## Run TypeSense scraper locally (locally the docs app should run on port 80, otherwise the scraping won't work correctly)

docker run --rm -it --env-file=scraper.development.env typesense/docsearch-scraper

## Run TypeSense on Staging

docker volume create typesense-data
docker run --name typesense -d --restart unless-stopped -p 8108:8108 -p 8107:8107 -v typesense-data:/data typesense/typesense:0.23.1 --data-dir /data --api-key=ssis_staging --enable-cors --cors-domains https://docs.staging.volley-labs.com