import React, { useState } from 'react';
import "./styles.css";
import {
  useNavbarMobileSidebar,
} from '@docusaurus/theme-common/internal';
import NavbarMobileSidebarToggle from '@theme/Navbar/MobileSidebar/Toggle';
import NavbarMobileSidebar from '@theme/Navbar/MobileSidebar';
import clsx from 'clsx';
import SearchBar from '@theme/SearchBar';
import NavbarSearch from '@theme/Navbar/Search';

function NavbarBackdrop(props) {
  return (
    <div
      role="presentation"
      {...props}
      className={clsx('navbar-sidebar__backdrop', props.className)}
    />
  );
}

// Link item
// {
//   href: process.env.REACT_APP_FRONTPAGE_URL,
//   label: "Cloud"
// }
const NavLink = ({ href, label }) => {
  return (
    <li className="nav-item">
      <a href={href} className="font-weight-bold">{label}</a>
    </li>
  );
};

const navLinks = [];

function Navbar() {
  const [openMobileMenu, setOpenMobileMenu] = useState(false);

  const activeClass = openMobileMenu ? 'active' : '';
  const toggleMobileMenu = () => setOpenMobileMenu((open) => !open);

  const mobileSidebar = useNavbarMobileSidebar();

  return (
    <nav className={clsx("navbar", mobileSidebar.shown && "navbar-sidebar--show")}>
      <div className="navbar-bg"></div>
      <div className="flex justify-space-between navbar-container">
        <a href="/" className="navbar-brand">
          <img src="/img/logo_bg.svg" alt="logo" className="logo" />
          <span className="name">COZYROC Cloud</span>
        </a>
        {navLinks.length > 0 && (
          <div className={`flex navbar-links-wrap ${activeClass}`}>
            <ul className="flex navbar-links-left navbar-links">
              {navLinks.map((item, i) => (<NavLink key={i} {...item} />))}
            </ul>
          </div>
        )}

        <div className="flex navbar-buttons">
          <NavbarSearch className="search-box">
            <SearchBar />
          </NavbarSearch>
          <a href={process.env.REACT_APP_MANAGEMENT_URL} className="font-weight-bold sign-in-link">Sign in</a>
          <div className="sign-up">
            <a href={`${process.env.REACT_APP_MANAGEMENT_URL}/auth/sign-up`} className="btn font-weight-bold atlas-cta cta-orange">Sign up</a>
          </div>
          {!mobileSidebar.disabled && <NavbarMobileSidebarToggle />}
          <NavbarBackdrop onClick={mobileSidebar.toggle} />
          <NavbarMobileSidebar />

          {navLinks.length > 0 && (
            <div className={`hamburger ${activeClass}`} onClick={toggleMobileMenu}>
              <span className="bar"></span>
              <span className="bar"></span>
              <span className="bar"></span>
            </div>
          )}
        </div>
      </div>

      <div className="collapse navbar-collapse justify-content-end" id="navbarNav">

      </div>
    </nav>
  );
}

export default Navbar;