
import React from 'react';
import "./styles.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF, faYoutube, faTwitter } from '@fortawesome/free-brands-svg-icons';

function Footer() {
  const year = new Date().getFullYear();

  return (
    <footer className="jumbotron jumbotron-fluid footer" id="copyright">
      <div className="container">
        <div className="row justify-content-between">
          <div className="col-md-6 col-sm-12 align-self-center text-center text-md-left text-lg-left my-2">
            <a href={`${process.env.REACT_APP_FRONTPAGE_URL}/AUP`}>Use Policy</a>
            <span> | </span>
            <a href={`${process.env.REACT_APP_FRONTPAGE_URL}/Terms`}>Terms of Service</a>
            <span> | </span>
            <a href={`${process.env.REACT_APP_FRONTPAGE_URL}/Privacy`}>Privacy Policy</a>
          </div>
          <div className="col-md-6 col-sm-12 align-self-center text-center text-md-right text-lg-right my-2" id="social-media">
            <a href="https://www.facebook.com/cozyroc" target="_blank" className="d-inline-block text-center ml-2 facebook">
              <i aria-hidden="true">
                <FontAwesomeIcon icon={faFacebookF} />
              </i>
            </a>
            <a href="https://twitter.com/cozyroc" target="_blank" className="d-inline-block text-center ml-2 twitter">
              <i aria-hidden="true">
                <FontAwesomeIcon icon={faTwitter} />
              </i>
            </a>
            <a href="https://www.youtube.com/playlist?list=PLzqEqJCeFHJtWDdTjb7xwbOHQFNpL35lR" target="_blank" className="d-inline-block text-center ml-2 youtube">
              <i aria-hidden="true">
                <FontAwesomeIcon icon={faYoutube} />
              </i>
            </a>
          </div>
          <div className="col-sm-12 align-self-center text-center text-md-right my-3">
            Copyright © {year} <a href="https://www.cozyroc.com/">COZYROC LLC</a>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;