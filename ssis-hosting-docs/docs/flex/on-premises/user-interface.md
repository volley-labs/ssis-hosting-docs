---
title: On-premises Agent User Interface
sidebar_label: User Interface
---
:::info
Available for Flex (SSIS) only
:::


1. Navigate to `localhost:10400` where `10400` is the port which you set up in step 2 during [on-premises agent installation](agent#install-the-agent).
2. To login into the application use the `password` which you set up in step 2 during [on-premises agent installation](agent#install-the-agent).

    ![img](/img/on-premises-agent-ui-login.png)

3. After successful login you will be redirected to the `Dashboard` page. It will contain:
   - the status of the `Connection` to COZYROC Cloud 
   - the count of the currently running `Executions`
   - the Agent `Service` status with `Restart` and `Stop` action buttons.
   
![img](/img/on-premises-agent-ui.png)


:::caution
If you click on `Restart` or `Stop` button while there are `running` executions, the agent service won't execute the action immediately, but will wait for all package executions to finish.
:::