---
title: Why Consider Flex On-Premises Plan?
sidebar_label: Use Cases
---

There are two main use cases for choosing COZYROC Cloud **Flex On-Premises** plan:

#### Conveniently manage on-premises environments

If your organization employs multiple SSIS execution environments, after installing a [COZYROC Cloud Agent](/flex/on-premises/agent) on each of your servers, you'll be able to manage these environments in a web-based control center. Additionally, if one of your servers is down (e.g. for maintenance), the COZYROC Cloud will automatically redirect scheduled executions to another on-premises environment, if available.

#### Hybrid event-driven integrations

An on-premises runtime is able to participate in hybrid event-driven scenarios. E.g. you can configure what inbound webhooks to consume from third-party apps and each [inbound webhook](/concepts-features/inbound-webhooks/overview) will trigger a package execution on your on-premises environment.
