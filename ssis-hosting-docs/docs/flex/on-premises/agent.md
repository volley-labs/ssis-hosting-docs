---
title: Cloud Agent
sidebar_label: Agent
---
:::info
Available for Flex (SSIS) only
:::

**COZYROC Cloud Agent** is a Windows service that is installed on each SSIS execution host and lets the COZYROC Cloud manage it. The installer can be downloaded from the web app when you sign in to your account. You can install an Agent on as many execution hosts as needed, but at least one is required before you are able to upload and execute packages.

### Installation

#### Generate an Access key and download installer

1. Navigate to the <a href={process.env.REACT_APP_MANAGEMENT_URL + "/agents/overview"} target="_blank">Agents</a> page. You can get to it via the `Executions Agents` link in the sidebar. If you haven't deployed any Agents yet, there will be a link to it on your `Dashboard`.
2. Click `GENERATE KEY` to create an `Agent access key`. Give it a meaningful name and confirm. After that you can copy the key from here when the `installer` asks for it. Access keys can be reused for several Agents.
3. Click the `GET AGENT` button to download the installer. 

![img](/img/generate_access_key.png)

#### Install the Agent

1. Start the `installer` and follow the steps to accept the `License agreement` and select the installation folder.
2. On the `Windows Service Configuration` step specify the following:
   - **Username**: Username that will be used to run the `Windows service`. The installer will automatically create a local user with the required permissions.
   - **Password**: Password for the service user.
   - **Port**: A free port for the `Agent service`. It **doesn't** need to be open to the public.

![img](/img/windows_service_config.png)

3. On the `Agent Cloud Settings` step, specify the following:
   - **Access Key**: Copy and paste the access key generated earlier.
   - **Agent Name**: A descriptive name to let you recognize the Agent in the Web application.
   - **Maximum parallel workers**: The number of executions that are allowed to run simultaneously on the Agent host. That number depends on your resources and needs. Default is 30.

![img](/img/agent_cloud_settings.png)

4. On the `Select Components` page, select the item(s) matching the `Microsoft SQL Server` version installed on your execution host.

![img](/img/select_agent_components.png)

5. Complete the installation by following the installer steps.

#### Verify Agent installation

Wait a couple of minutes and refresh the `Agents` page in the web app. Your Agent should appear as `ONLINE`. Now you can start uploading packages.

![img](/img/agent_online.png)

Alternatively you can open [on-premises agent user interface](user-interface) and ensure the service is running.

### Execution machine maintenance

Prior to doing any sort of maintenance on an execution machine ( like installing Windows updates ) the `COZYROC Cloud Agent` should be stopped safely.

1. Use the [on-premises agent user interface](user-interface) to stop the Agent. It will stop after all currently running executions are completed.
2. Do the maintenance activities.
3. Start the `COZYROC Cloud Agent` service from `Windows Service Manager`.

### Configuration changes and updates

The process of both changing the Agent configuration and applying updates is the same.

1. Download the Agent installer.
2. **Important:** Stop the `COZYROC Cloud Agent` service from [on-premises agent user interface](user-interface) to ensure no executions will be accepted or interrupted during the update.
3. Start the installer and follow the steps. Change configuration values, if needed.
4. The installer will automatically start the Agent service at the end.

#### Agent self-update (recommended)

Since version `2.1.1.7609` of on-premises agent you can start self-update procedure through
the management app when there is a newer version of the agent and your agent is `Online`. We recommed you to use this method to update your agents, because it reduces the risk of data loss by waiting for currently running executions to finish before starting the update procedure.
In that case you will see `update agent` button at the beginning of on-premises agents table row and a `warning` icon in front of your agent version as shown below.

![img](/img/agent-self-update.png)

If your agent version is less than `2.1.1.7609` you can still click on update agent button
but you will see a warning dialog that your agent version doesn't support self-update.
In that case you have to follow the instructions from the dialog and download latest agent version
and install it manually.

![img](/img/agent-self-update-not-supported.png)

When you click on `update agent` button and if your agent version is eligible for update then your agent
status will change from `Online` to `On Hold`. The update procedure may take few minutes to complete.
Next you can hit the `refresh button` and if the procedure is successful you will see that your agent version
is updated and your agent is back `Online`.