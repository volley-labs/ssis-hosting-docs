---
title: Flex Billing
sidebar_label: Billing
pagination_label: Flex (SSIS) Billing
---

When you sign up for a COZYROC Cloud Flex (SSIS) account, you will have 14-days free trial initially. You can choose between `Flex Managed` and `Flex On-Premises` plans then select a **monthly** or an **annual** subscription (you get 20% off with an annual subscription) in the <a href={process.env.REACT_APP_MANAGEMENT_URL + "/subscription"} target="_blank">Subscription page</a>.

![img](/img/flex-trial-subscription.png)

### Free trial

Your 14-day trial starts upon confirming your e-mail address. While your trial is active, you can use all of the features of Flex (SSIS) service. When you are ready to become a paid customer, go to the <a href={process.env.REACT_APP_MANAGEMENT_URL + "/subscription"} target="_blank">Subscription page</a> and enter your payment details. After your trial expires, your data will still be accessible, but you won't be able to execute packages (neither scheduled, nor on-demand, nor via webhook).

### Invoices and payments

The billing and payment collection is handled by [Stripe](https://stripe.com/) and [Zoho Subscriptions](https://www.zoho.com/subscriptions/). From your <a href={process.env.REACT_APP_MANAGEMENT_URL + "/subscription"} target="_blank">Subscription page</a>, you will have access to a [Zoho Subscriptions customer portal](https://www.zoho.com/subscriptions/help/customer-portal-functions.html), from where you can view and download information about your invoices and payments.