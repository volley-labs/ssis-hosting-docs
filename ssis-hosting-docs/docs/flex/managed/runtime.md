---
title: Managed Environment
sidebar_label: Overview


---

COZYROC Cloud Managed Environment offers a secure and affordable SSIS execution runtime in the cloud, that spares your team from doing manual installations, software and hardware maintenance, upgrades of SSIS and COZYROC SSIS+, etc.

---

*COZYROC Cloud Managed Runtime* is similar to [Microsoft Azure SSIS Integration Runtime](https://docs.microsoft.com/en-us/azure/data-factory/concepts-integration-runtime), but has several notable advantages:

| | COZYROC Cloud MR | Azure SSIS IR |
| ------ | ------ | ------ |
| Initial setup time | Several minutes | Several days |
| Manageability | Single web app with a simple, intuitive UI | Complex to properly configure and monitor |
| Third-party SSIS addon(s) | [COZYROC SSIS+](/flex/managed/ssis+/) included | Separately purchased addon(s) |
| Near real-time integration | Easily supported (via inbound webhooks) | Difficult to set up in ADF. Expensive to run |
| Pricing | Straightforward and predictable pricing. Very affordable | Significantly more expensive |