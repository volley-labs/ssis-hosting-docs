---
title: COZYROC SSIS+ Addon
sidebar_label: COZYROC SSIS+ Addon
---

:::info
A **COZYROC SSIS+** installation is available only in the **managed** execution environment included in the [Flex Managed](/flex/plans/#flex-managed-plan) subscription plan. If you want to use SSIS+ components in your on-premises environment, you have to purchase a license separately [here](https://www.cozyroc.com/purchase).
:::

Automating real-world integration scenarios usually requires more capabilities than what is provided by Microsoft's "out-of-the-box" SQL Server Integration Services. [COZYROC SSIS+](http://cozyroc.com/products) adds **200+ advanced tasks and components** to your SSIS toolbox to help you meet your project requirements without having to write and maintain a lot of custom code.

By having **COZYROC SSIS+** pre-installed on a **managed SSIS execution server**, you will be able to use the following functionality and much more:

#### Application connectivity
* [Advanced REST Framework](http://cozyroc.com/ssis/rest) - use pre-built configurations or author your custom configurations to easily consume virtually any Web API.
* Advanced components for interfacing with popular business apps and services: [Salesforce](http://cozyroc.com/ssis/salesforce), [Dynamics CRM](http://cozyroc.com/ssis/dynamics-crm), [NetSuite](https://www.cozyroc.com/ssis/netsuite), [SharePoint](http://cozyroc.com/ssis/sharepoint), etc.
* [Electronic Data Interchange (EDI)](http://www.cozyroc.com/ssis/edi) processing.
* [Microsoft Active Directory/LDAP](http://cozyroc.com/ssis/ldap) connectivity.

#### Development productivity
* [Dynamic Data Flows](http://cozyroc.com/ssis/data-flow-task) - overcome the SSIS limitation of working with static metadata.
* [Database Destination](http://cozyroc.com/ssis/database-destination) - bulk load & upsert for all major databases.
* [JavaScript Scripting](http://cozyroc.com/ssis/javascript) - easily author and maintain custom SSIS scripts with JS.

#### Miscellaneous 
* Secure advanced email support (SMTP, IMAP, POP3, Exchange)
* Secure file transfer protocol (SFTP, FTPS)
* Zip, GZip, BZip2, Tar compression and decompression
* ... and much more
