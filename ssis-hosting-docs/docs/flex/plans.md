---
title: Flex Subscription Plans
sidebar_label: Plans
pagination_label: Flex (SSIS) Plans
---

<a href={process.env.REACT_APP_FRONTPAGE_URL + "/flex"} target="_blank">COZYROC Cloud</a> Flex (SSIS) service comes with two plans, each with an **yearly** or a **monthly** commitment. 

:::info Pricing
You can find information about pricing <a href={process.env.REACT_APP_FRONTPAGE_URL + "/flex/pricing"} target="_blank">here</a>.
:::

### Flex On-Premises plan

The **On-Premises** plan allows for running SSIS packages only on your own self-hosted SSIS execution environment(s). It supports:
* Web-based adminstration and monitoring of SSIS executions
* Unlimited [SSIS packages](/flex/packages/overview) & [scheduled jobs](/concepts-features/scheduled-jobs) & [inbound webhooks](/concepts-features/inbound-webhooks/overview)
* Unlimited [collaborators](/concepts-features/users) on your team
* 15 days of execution log retention

:::info Note
A **COZYROC SSIS+** license is not included in the subscription. If you want to use SSIS+ components in your on-premises environment, you have to purchase a license separately [here](https://www.cozyroc.com/purchase).
:::

### Flex Managed plan

The **Managed** plan includes everything offered in the **On-Premises** plan, and in addition provides a managed (by COZYROC) SSIS execution environment, where [COZYROC SSIS+](/flex/managed/ssis+) addon tasks and components are readily available. The managed environment processing resources are shared, but tenants' data is isolated.

The restrictions of the managed SSIS execution environment in the **Managed** plan are:
* 100 execution hours/month
* Single job duration up to 30 minutes
* Free 10GB/month of network traffic. Extra traffic charged $0.25 per 1GB
* 1GB storage for temporary execution files

If your company has special needs, please contact us at sales@cozyroc.cloud for a custom quote.

:::info Note
The **Managed** plan of COZYROC Cloud service works with a hosted MS SQL Server 2019 Standard Edition. Please note that SSIS comes with Microsoft SQL Server and it is not offered separately. As COZYROC is a Microsoft SPLA partner, according to your SPLA SQL Server license, you will also get access to any SQL Server features besides SSIS.
:::
