---
title: Flex Overview
sidebar_label: Overview
pagination_label: Flex (SSIS) Overview
---

The Flex service is powered by [SQL Server Integration Services](https://docs.microsoft.com/en-us/sql/integration-services/sql-server-integration-services) (the renowned enterprise-grade ETL platform by Microsoft). You can easily upload SSIS packages, schedule their execution, automatically run packages by subscribing to external events via inbound webhooks, monitor the execution progress and get notified about problems.

COZYROC Cloud supports all SSIS package configuration options that you are used to and builds upon them to improve your experience. You can explore the package parameterization basics [here](/concepts-features/executions/parameterization#flex-ssis) or delve into advanced topics like [environments](/concepts-features/environments) and extracting data from [webhooks](/concepts-features/inbound-webhooks/call-webhook-with-request-data). 

![img](/img/parameter-editor/parameter-editor.png)

Besides package execution orchestrator, <a href={process.env.REACT_APP_FRONTPAGE_URL + "/flex"} target="_blank">COZYROC Cloud</a> can also be an end-to-end [iPaaS](https://en.wikipedia.org/wiki/Cloud-based_integration) solution, by providing a [managed SSIS execution environment](/flex/managed/runtime), effectively eliminating the hassle of hardware and software infrastructure maintenance. Packages executed in our managed environment can leverage the full power of the [COZYROC SSIS+](/flex/managed/ssis+) 200+ components.

![img](/img/execution-overview.png)

If you have already invested a lot of resources in your own environment, but you want to use some of COZYROC Cloud's advanced features like [inbound webhooks](/concepts-features/inbound-webhooks/overview), you can install one or multiple [On-premises](/flex/on-premises/why) execution agents. By doing this you keep the on-premises flexibility while gaining a powerful centralized management interface for your SSIS workloads.

![img](/img/agent_online.png)




