---
title: Automatic Variables
sidebar_label: Automatic Variables
---

:::info
Available for Flex (SSIS) only
:::

While executing packages in the [managed](/flex/managed/runtime) environment, you can make use of system resources (like disk space) according to your [billing plan](/flex/plans). To enable easier access to these resources COZYROC Cloud will detect some [user-defined variables](https://learn.microsoft.com/en-us/sql/integration-services/integration-services-ssis-variables) in your package based on a naming convention and set their values.

### Currently supported automatic variables

| Variable name | Automatic value |
| ------ | ------ |
| CC_TEMP_DIR | Full path to your managed temp folder |

### Example

You want to create a temporary file `data.csv` during execution.

1. Create a SSIS package
2. Create a package variable called `CC_TEMP_DIR` with data type `String`
3. Set the variable default value to a folder on your machine so you can use it during development. The value will later be overwritten by COZYROC Cloud
4. Create a `File Connection` in mode `Create file`.
5. Right-click the connection select `Properties` and then `Expressions`
6. For `Property` select `ConnectionString`
7. For `Expression` set `@[User::CC_TEMP_DIR] + "\\data.csv"`
8. Finalize any additional logic in your package
9. Upload package to COZYROC Cloud
10. Execute the package. The `data.csv` file will be placed in your managed temp folder

:::note
Any files created during an execution in the managed environment are deleted after the execution ends.
:::
