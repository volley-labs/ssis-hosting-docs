---
title: Packages Overview
sidebar_label: Overview
---

:::info
Available for Flex (SSIS) only
:::

import YouTubePlayer from "../../../src/components/YouTubePlayer/YouTubePlayer.jsx";

[SSIS packages](https://docs.microsoft.com/en-us/sql/integration-services/integration-services-ssis-packages) encode the functionality for an integration scenario by using reusable and customizable building blocks - SSIS Connection managers, Control Flow elements, Data Flow elements, etc.

## Developing SSIS packages

SSIS packages are developed in Visual Studio, where they can be locally tested. When they are ready to be deployed, they can be uploaded in the web app.

**NOTE:** For the shared managed execution environment (from the **Flex Managed plan**), please see the recommendations below: 

* It's recommended that the target for your packages be set to *SQL Server 2019*, as the execution engine uses SQL Server 2019. The automatic upgrade of SSIS packages is not guaranteed to always work properly. 
* It's recommended to use either *Visual Studio 2019* or *Visual Studio 2017*, both of which support SQL Server 2019 as a target.
* It's recommended to use the same version of [COZYROC SSIS+](/flex/managed/ssis+) library for developing SSIS packages in Visual Studio, that matches the version used in COZYROC Cloud execution environment. Please also check the [COZYROC SSIS+ to SSDT compatibility matrix](https://desk.cozyroc.com/portal/en/kb/articles/installation-and-getting-started) for other compatibility considerations.

## Uploading SSIS packages

You can reach <a href={process.env.REACT_APP_MANAGEMENT_URL + "/package/upload"} target="_blank">the page for uploading a package</a> via *Packages > Upload Package*, via the *Dashboard* page or [using Visual Studio extension](/how-to/vs-deploy). There, you'll need to select the package for upload and, optionally, specify a folder name. If the sensitive data in the package is password protected, it is recommended that you specify the password when uploading the package; otherwise, you'll need to manually input all the sensitive values later.

<YouTubePlayer embedUrl="https://www.youtube.com/embed/bzk-olGQY2k" />

### Uploading new SSIS package version

If you updated your package in Visual Studio and you want to update it in `COZYROC Cloud` as well, you can do this by 
navigating to the package details page and clicking on `Upload new version` button as shown below.

![img](/img/packages/upload-new-version.png)

You will be redirected to the usual package upload page with predefined readonly name and folder. Select your new package file, set password if necessary and click `Save`.

During the upload process parameters and connection managers from the new package will be compared to the currently existing scheduled jobs and inbound webhooks parameters.
If any of the following cases is detected for:
1. **Parameters**
- new parameter
- missing parameter
- parameter sensitivity changed
- parameter data type changed

2. **Connection managers**
- new connection manager
- missing connection manager
- changed connection parameters in existing connection manager

Then you will be notified via email and you will be able to `confirm` or `discard` the changes from the package details page.

![img](/img/packages/upgrade-package-conflicts.png)

## Scheduling SSIS package executions

After uploading a package, you can create a [scheduled job](/concepts-features/scheduled-jobs) to run it on a regular basis. You might need to [parameterize the execution](/concepts-features/executions/parameterization) first. We recommend that you do a test run and inspect the logs before scheduling executions.

![img](/img/add-trigger.png)

## Creating package webhook
You can create an [inbound webhook](/concepts-features/inbound-webhooks/overview) and call it outside of COZYROC Cloud application via HTTP GET or HTTP POST request. When you call the webhook url it will trigger package execution.

![img](/img/add-trigger.png)