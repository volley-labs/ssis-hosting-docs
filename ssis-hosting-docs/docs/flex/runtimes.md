---
title: Runtime Environments
sidebar_label: Runtime Environments
---

An SSIS package that has been uploaded in COZYROC Cloud can be executed in both:
* a managed SSIS execution server
* an on-premises SSIS execution server

Please find below a comparison chart of the two types of execution environments:

|              | Managed runtime | On-Premises runtime |
| -----------  | ----------- | ----------- |
| Maintained by | COZYROC | Client |
| Benefits | No-hassle, cost-effective, shared runtime. | Full-control over what's installed. Direct access to on-premises resources. |
| Available in | [Flex Managed](/flex/plans)| [All plans](/flex/plans) |
| SSIS Licensing | Via SPLA service provider * | Client's responsibility |
| Target Version | SQL Server/SSIS 2019 | Any SQL Server/SSIS versions (2012, 2014, 2016, 2017, 2019, 2022) that are installed on client's premises |
| Addons | Only [COZYROC SSIS+](/flex/managed/ssis+) | Any third-party SSIS components or custom DLLs can be installed |
| COZYROC SSIS+ | Included in the [Flex Managed plan](/flex/plans) | Purchase [separately](https://cozyroc.com/purchase) |
| Prerequisites | none | Installation of an [agent](/flex/on-premises/agent) |


\* The SQL Server Integration Services is licensed with the help of a SPLA service provider of Microsoft SQL Server hosting services. COZYROC LLC is a certified SPLA service provider.
