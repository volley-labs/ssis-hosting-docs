---
title: Security & Data Privacy
sidebar_label: Security & Data Privacy
---

To protect the data in our customers' accounts, we employ the following security best practices:
* All sensitive data is encrypted in our databases with 256-bit AES encryption
* All network communication is secured with TLS 1.2
* Strict access controls and procedures are implemented.
* Set up [SSH connectivity](/concepts-features/connectivity/ssh-configurations) for extra security
* [Multi-factor authentication](/concepts-features/multi-factor-authentication)

Please also refer to <a href={process.env.REACT_APP_FRONTPAGE_URL + "/Privacy"} target="_blank">COZYROC Cloud Privacy Policy</a>