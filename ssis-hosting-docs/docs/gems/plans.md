---
title: Gems Subscription Plans
sidebar_label: Plans
pagination_label: Gems (Pre-built) Plans
---
<a href={process.env.REACT_APP_FRONTPAGE_URL} target="_blank">COZYROC Cloud</a> Gems (Pre-built) service comes with two plans, a free plan and a paid plan with an **yearly** or a **monthly** commitment. 

:::info Pricing
You can find information about pricing <a href={process.env.REACT_APP_FRONTPAGE_URL + "/gems/pricing"} target="_blank">here</a>.
:::

### Gems plan

The **Gems** plan allows for easily configurable off-the-shelf integrations. It supports:
* Web-based adminstration and monitoring of gem installations and executions
* Unlimited gem installations, [scheduled jobs](/concepts-features/scheduled-jobs) and [inbound webhooks](/concepts-features/inbound-webhooks/overview)
* Unlimited [collaborators](/concepts-features/users) on your team
* 15 days of execution log retention

The restrictions of the **Gems** plan are:
* 100 execution hours/month
* Single execution duration up to 30 minutes
* Free 10GB/month of network traffic. Extra traffic charged $0.25 per 1GB

### Free plan

The **Free** plan allows you to manually trigger on-demand gem runs.

The restrictions of the **Free** plan are:
* 1 execution hour/month
* Single execution duration up to 30 minutes
* Up to 1GB/month of network traffic.

:::note
If your company has special needs, please contact us at sales@cozyroc.cloud for a custom quote.
:::