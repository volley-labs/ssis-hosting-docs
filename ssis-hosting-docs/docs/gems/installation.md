---
title: Gems Installation
sidebar_label: Installation
pagination_label: Gems (Pre-built) Installation
---

# Installation

After selecting the GEM you would like to install, click the `INSTALL` button. You will then be redirected to the installation page, as shown below.

![img](/img/gems/installation-wizard.png)

**On that page you will have two options:**

## 1. Follow the installation instructions (Recommended)

If you choose to follow the installation instructions, your input will be validated at each step when you click the `NEXT` or `PREV` buttons. You will also see a helpful description for each parameter on the right side of the screen.

![img](/img/gems/wizard-instructions.png)

After configuring all steps, click the `INSTALL` button to install the GEM. Once the installation is successful, you’ll be prompted to create a trigger for the installed GEM, or you can skip this step and proceed to the GEM details page.

![img](/img/gems/successful-gem-install.png)

## 2. Do a quick install

By clicking the `INSTALL >>` button, you will be asked to confirm whether you would like to proceed with a quick install and set up your parameters later.

![img](/img/gems/quick-install.png)

Once you confirm, the GEM will be installed.
