---
title: Gem Workflow
sidebar_label: Gem Workflow 
pagination_label: Gem Workflow
---

:::info
Available for Partners only
:::

**This page outlines all gem version states together with the actions that lead to them.**

![img](/img/gems/partners/gem-flow.jpeg)

## Draft

`Draft` is the initial state of every gem version. In this state all its attributes can be changed as many times as needed. A draft version is not installable and won't be visible anywhere outside of your workspace. Once the draft is completed use the `Finalize` action to change its state to `Private`.

## Private

A `Private` gem version is closed for changes. It is installable, but it is not available in the public gem gallery. Users outside of your organization can see the gem version, if you `share` it with them. `Publish` the gem version to make it publicly available. It will go through a mandatory `review` process before it is displayed in the gallery.     

## In Review

The gem version will remain `In Review` until COZYROC `approves` or `rejects` it. If you want to make further changes, `Re-open` the gem version. In this case the approval process will be interrupted and the version will be moved back to `Draft`. While `In Review` your gem version is not installable.

## Rejected

If a gem version is not suitable for the public gallery, it will show up in your workspace as `Rejected`. COZYROC will provide feedback for every rejection and request changes. `Re-open` the rejected gem version as `Draft` to resolve all issues and repeat the workflow. 

## Published

When your gem version passes the review process successfully its status will change to `Published` and it will start showing up in gallery searches. Published version can be found and installed by any organization with access to the gallery. You can `Unpublish` a gem version from the gallery by providing a notice with the reason for your decisions. This action won't affect existing installations, but the gem version will be moved to `Private` and won't be available in the gallery anymore.
