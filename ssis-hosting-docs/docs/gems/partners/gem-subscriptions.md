---
title: Gem Subscriptions
sidebar_label: Gem Subscriptions 
pagination_label: Gem Subscriptions
---

:::info
Available for Partners only
:::

As a COZYROC Cloud Partner you are able to earn revenue on the platform based on the active subscriptions to the gem subscriptions to the gems you developed. Gem subscription payments will be collected by COZYROC and distributed to you once a month. The <a href={process.env.REACT_APP_MANAGEMENT_URL + "/partner/subscriptions"} target="_blank">Gem Subscriptions</a> page gives a detailed overview of current gem subscriptions in a certain period of time together with the total earned revenue. Additionally you can generate the report for specific gem, subscription status and customer.

![img](/img/gems/partners/gem-subscriptions.png)