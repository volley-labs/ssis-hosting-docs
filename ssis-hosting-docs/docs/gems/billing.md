---
title: Gems Billing
sidebar_label: Billing
pagination_label: Gems (Pre-built) Billing
---

When you sign up for a COZYROC Cloud Gems (Pre-built) account, you will have 14-days free trial initially. You can choose `Gems` plan and select a **monthly** or an **annual** subscription (you get 20% off with an annual subscription) in the <a href={process.env.REACT_APP_MANAGEMENT_URL + "/subscription"} target="_blank">Subscription page</a>.

:::note
After your trial expires you will be automatically switched to the `FREE` plan. Your data will still be accessible, but you will have a limited functionality. Only manual gem runs will be allowed.
:::

![img](/img/gems-trial-subscription.png)

### Free trial

Your 14-day trial starts upon confirming your e-mail address. While your trial is active, you can use all of the features of Gems (Pre-built) service. When you are ready to become a paid customer, go to the <a href={process.env.REACT_APP_MANAGEMENT_URL + "/subscription"} target="_blank">Subscription page</a> and enter your payment details.

### Invoices and payments

The billing and payment collection is handled by [Stripe](https://stripe.com/) and [Zoho Subcriptions](https://www.zoho.com/subscriptions/). From your <a href={process.env.REACT_APP_MANAGEMENT_URL + "/subscription"} target="_blank">Subscription page</a>, you will have access to a [Zoho Subcriptions customer portal](https://www.zoho.com/subscriptions/help/customer-portal-functions.html), from where you can view and download information about your invoices and payments.