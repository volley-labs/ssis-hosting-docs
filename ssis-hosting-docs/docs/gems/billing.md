---
title: Gems Billing
sidebar_label: Billing
pagination_label: Gems (Pre-built) Billing
---

When you sign up for a COZYROC Cloud Gems (Pre-built) account, you will have 14-days free trial initially. You can choose `Gems` plan and select a **monthly** or an **annual** subscription (you get 20% off with an annual subscription) in the <a href={process.env.REACT_APP_MANAGEMENT_URL + "/subscription"} target="_blank">Subscription page</a>.

:::note
After your trial expires you will be automatically switched to the `FREE` plan. Your data will still be accessible, but you will have a limited functionality. Only manual gem runs will be allowed.
:::

![img](/img/gems-trial-subscription.png)

### Free trial

Your 14-day trial starts upon confirming your e-mail address. While your trial is active, you can use all of the features of Gems (Pre-built) service. When you are ready to become a paid customer, go to the <a href={process.env.REACT_APP_MANAGEMENT_URL + "/subscription"} target="_blank">Subscription page</a> and enter your payment details.

### Gem subscriptions

Billing for any individual gem you install into your COZYROC Cloud account functions like a small monthly subscription attached to your main COZYROC Cloud subscription. You will receive a separate monthly invoice for each of your gem installations that requires payment. 

#### Free Gems

A lot of the gems in the COZYROC Cloud Gallery are included in your main subscription and free for you to use at any time. You can easily spot them in the Gallery where they directly have an `Install` action or by inspecting their details page where the pricing is displayed as `Free`.

![img](/img/gems/gem-free.png)

![img](/img/gems/gem-free-details.png)

#### Premium Gems with trial period

Some gems require a monthly payment, but offer several days trial period so you can evaluate the gem functionality. In the Gallery such gems have the `Start Trial` action and on the details page you will be able to see the exact price and number of trial days.

![img](/img/gems/gem-paid-with-trial.png)

![img](/img/gems/gem-paid-with-trial-details.png)

#### Premium Gems without trial period

Other gems require payment from the start and your credit card will be immediately charged upon installation. Such gems directly state their price in the Gallery as well as on their details page.

![img](/img/gems/gem-paid.png)

![img](/img/gems/gem-paid-details.png)

#### Viewing your gem subscriptions

On your <a href={process.env.REACT_APP_MANAGEMENT_URL + "/subscription"} target="_blank">Subscription page</a> there is a list of your gem subscriptions together with all relevant information: status, last billed date, next billing date etc.

![img](/img/gems/gem-subscriptions.png)

In case you believe that your credit card was charged without a reason click on the `Dispute` button of the relevant gem subscription and send us a request. The COZYROC Support Team will contact you shortly.

![img](/img/gems/gem-subscriptions-dispute.png)

![img](/img/gems/gem-dispute.png)

If your credit card is invalid or your credit card issuer declines the payment, your gem subscription will enter a `Payment declined` state. When you resolve your credit card issues click on the `Retry payment` button.

![img](/img/gems/gem-subscriptions-retry.png)

### Invoices and payments

The billing and payment collection is handled by [Stripe](https://stripe.com/) and [Zoho Subcriptions](https://www.zoho.com/subscriptions/). From your <a href={process.env.REACT_APP_MANAGEMENT_URL + "/subscription"} target="_blank">Subscription page</a>, you will have access to a [Zoho Subcriptions customer portal](https://www.zoho.com/subscriptions/help/customer-portal-functions.html), from where you can view and download information about your invoices and payments.

