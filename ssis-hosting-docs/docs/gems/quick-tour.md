---
title: Gems Quick Tour
sidebar_label: Quick Tour
pagination_label: Gems (Pre-built) Quick Tour
---

The COZYROC Cloud web application allows you to manage gems, scheduled jobs, inbound webhooks, executions, your subscription, your profile and other users in your account. It works well on both desktop and mobile browsers.

![img](/img/gems/dashboard.png)

Once you have logged into the COZYROC Cloud, you’ll see the following menu items on the left side:
* **Dashboard** - The page shows you some high-level information about your account such as the number of installed gems, the number of scheduled jobs and the number of inbound webhooks. There's also information about executions in a period of your choice (all, last day, last week, last month). The information you will see for the period you choose is the number of execution jobs by status. For more details about execution statuses see [here](/concepts-features/executions/overview#execution-statuses). On the Dashboard, you'll also see the number of environments that have been set up.
* **Gems** - The page shows installed gems and a gallery with pre-built gems which you can select and install.
* **Scheduled Jobs** - This page shows jobs that have been scheduled in the past as well as jobs that are currently scheduled to run at some point in the future. It includes both recurring and one-time jobs. You can also create a scheduled job from an already installed gem. Read about scheduled jobs [here](/concepts-features/scheduled-jobs).
* **Webhooks** - This page shows webhooks that have been created. You can also create an inbound webhook from an already installed gem. Read about inbound webhooks [here](/concepts-features/inbound-webhooks/overview).
* **Executions** - On this page you will see a list of executed jobs, inbound webhooks or executions that have been triggered manually. The list includes the ID, the status (usually succeeded or failed), the name of the gem, the time the execution started, the trigger, and the duration. You can filter jobs by time period (all, last day, last week or last month). To see the execution log, click on the ID or the status of the execution. Read about executions [here](/concepts-features/executions/overview).
* **Environments** - This page shows the [environments](/concepts-features/environments) that have been set up. This is also where you would start to set up a new environment.
* **Connectivity** This page shows the [SSH configurations](/concepts-features/connectivity/ssh-configurations) that have been set up, as well as [REST token files](/concepts-features/connectivity/rest-tokens/overview) that have been uploaded. This is also where you would create new SSH configuration and create or upload new REST token.
* **Users** - This page shows all the users who are granted access to your COZYROC Cloud account. It shows the email address, first and last name and the role(s) given to each user. It also displays whether that user’s account has been locked or suspended. Read more about user management [here](/concepts-features/users).
* **Audit Log** - On this page, you can see all the actions that have been added, modified or deleted data in the account. You can optionally filter them by type and user. For non-admin users the menu will be "My activities". Read more about the audit log [here](/concepts-features/users#audit-log).

On the top-right corner, you'll see a menu with the following items:
* **Profile** - read more about [managing your profile](/concepts-features/profile).
* **Subscription** - general information about your subscription
* **Sign out** - log out of the application
