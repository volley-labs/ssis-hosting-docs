---
title: Gems Overview
sidebar_label: Overview
pagination_label: Gems (Pre-built) Overview
---
:::info
Available for Gems (Pre-built)
:::

Gems are easily configurable off-the-shelf integrations with popular SaaS applications. To get started you just have to:

* Find the gem that you need in the Gems Gallery
* Configure it and do a trial run
* Specify a schedule or a webhook trigger

![img](/img/gems/gallery-overview.png)

## Gallery
On the **Gallery** tab you can:
* Add trigger to start gem execution 
* [Install a gem](/gems/installation)
* See the version of the gem
* See how many times the gem was installed
* Upgrade gem to the latest version

![img](/img/gems/gem-overview-actions.png)

## Installed

On the **Installed** tab you can:
* See all of the installed gems
* Add trigger to start gem execution
* Upgrade gem to the latest version

![img](/img/gems/gems-overview-installed.png)

## Gem details

On the installed gem details page you can:
* View more details about the gem
* Configure required parameters
* Add trigger
* Run gem manually to test parameters configuration
* Uninstall the gem
* View gem versions history

![img](/img/gems/installed-gem-details.png)

:::info
Most of the SaaS platforms will require a REST Token for authentication. For detailed description how to create a REST Token in COZYROC Cloud please refer to [this article](/concepts-features/connectivity/rest-tokens/create).
:::

## Gem versions

All available versions of a Gem are listed in the `Versions` tab on the gem's page. If you want to update to a newer version or switch to an older one, you need so select the desired version from the list and click on the `Switch to this version` button. Only one version of a particular gem can be installed at a time. You will be notified, if any of your existing triggers needs changes to work of the new gem version.

![img](/img/gems/gem-versions.png)
![img](/img/gems/switch-gem-version.png)
