---
id: welcome
title: Welcome to COZYROC Cloud
sidebar_label: Welcome
slug: /
pagination_next: null
---

<a href={process.env.REACT_APP_FRONTPAGE_URL} target="_blank">COZYROC Cloud</a> offers:

## Gems (Pre-built)
 
Easily configurable integrations with popular SaaS applications. Enjoy a no-hassle workflow automation on an affordable Integration Platform as a Service!

Take the [Gems (Pre-built) quick tour](/gems/quick-tour).

## Flex (SSIS)

Automate any integration scenario using custom-built SSIS packages. Offers cloud-based data orchestration for on-premises SSIS execution environments ("Flex On-Premises") and a fully managed execution environment ("Flex Managed").

Take the [Flex (SSIS) quick tour](/flex/quick-tour)

:::note 
Upon sign-up for your trial you will have to choose between [Gems (Pre-built)](/gems/quick-tour) and [Flex (SSIS)](/flex/quick-tour) plan.
:::
