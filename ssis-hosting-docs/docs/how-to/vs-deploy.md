---
title: Deploy from Visual Studio
sidebar_label: Deploy from Visual Studio
---

:::info
Available for Flex (SSIS) only
:::

## Overview

The Visual Studio extension that comes with the latest COZYROC SSIS+ installers allows deploying [SSIS packages](https://learn.microsoft.com/en-us/sql/integration-services/integration-services-ssis-packages) to <a href={process.env.REACT_APP_MANAGEMENT_URL} target="_blank">COZYROC Cloud</a>. Right click on an SSIS package in Solution Explorer to bring up the context menu and then click `Deploy to COZYROC Cloud...`.

![img](/img/vs-addon/menu.png)

## Login

First you have to sign in using COZYROC Cloud credentials. If you don't have an account you could <a href={process.env.REACT_APP_MANAGEMENT_URL + "/auth/sign-up"} target="_blank">sign up</a> for a free trial.

![img](/img/vs-addon/login-form.png)

## Deploy package

Once you have signed-in, you'll see the following form:

![img](/img/vs-addon/upload-form.png)

Here you could change the display name of the selected package and the target folder.

If the package is encrypted with user key (i.e. package protection level is `EncryptSensitiveWithUserKey` or `EncryptAllWithUserKey`) and you want to send also the sensitive data to COZYROC Cloud, you can check the re-encrypt option and specify a password. Otherwise click on `Save` option.

![img](/img/vs-addon/upload-form-pass.png)
 
Upon successful deployment of the package, you will be redirected to the web app to see the package details page. There you can amend its [parameters](/concepts-features/executions/parameterization), [scheduled jobs](/concepts-features/scheduled-jobs), [webhook triggers](/concepts-features/inbound-webhooks/overview).

![img](/img/vs-addon/details.png)