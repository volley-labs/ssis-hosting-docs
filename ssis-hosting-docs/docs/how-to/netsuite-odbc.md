---
title: How to use NetSuite ODBC driver
sidebar_label: Use NetSuite ODBC
---

:::info
Available for Flex Managed (SSIS) only
:::

In addition to providing components for using [NetSuite SOAP API](https://www.cozyroc.com/ssis/netsuite) and [NetSuite REST API](https://cozyroc.com/ssis/rest-netsuite-connection), COZYROC Cloud [managed](/flex/managed/runtime) environment supports also NetSuite ODBC connectivity, using the official drivers provided on the NetSuite website.

:::note
COZYROC Cloud supports only **DSN-less** ODBC connections, which means you'll have to provide all needed connection information in the connection string.
:::

### Available drivers

* NetSuite Drivers 64bit
* NetSuite Drivers 32bit

### Establishing connection

1. Enable the SuiteAnalytics Connect Service for your NetSuite account as described [here](https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_3996274388.html#To-enable-the-Connect-Service-feature%3A)
2. Get your `ServiceHost`, `accountID` and `roleID` from  your NetSuite account's Configuration page as described [here](https://docs.oracle.com/en/cloud/saas/netsuite/ns-online-help/section_4406003916.html#bridgehead_4406670604) under the `DSN-less connection` section.
3. In your package use a connection string with the following format:
```
DRIVER=NetSuite Drivers 64bit;Host=<ServiceHost>;Port=1708;Encrypted=1;AllowSinglePacketLogout=1;Truststore=system;SDSN=NetSuite.com; UID=<username / email>;PWD=<password>;CustomProperties=AccountID=<accountID>;RoleID=<roleID> 
```

### Execution runtime

COZYROC Cloud allows 64 bit as well as 32 bit executions. A runtime switch is available for all package triggers.

![img](/img/how-to/runtime-switch.png)

Depending on what runtime you plan to use you need to set the corresponding ODBC driver property (`DRIVER`) in your connection string.

- For 64 bit executions use `NetSuite Drivers 64bit`
- For 32 bit executions use `NetSuite Drivers 32bit`
