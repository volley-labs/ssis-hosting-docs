---
title: Expose Azure SQL Database to COZYROC Cloud
sidebar_label: Expose Azure SQL Database to COZYROC Cloud
---

:::info
Applies to Flex (SSIS) and Gems (Pre-built)
:::

By default Azure will block COZYROC Cloud from connecting to your SQL databases. In order to execute your package or gem successfully you need to configure the Azure SQL Server firewall first. Follow the steps below to allow COZYROC Cloud to access your databases:

1. Open your SQL Database in Azure and click on `Set server firewall` in the Overview.

![img](/img/how-to/set-server-firewall.png)

2. Under `Security` - `Networking` - `Public access` scroll to `Firewall rules` and click `Add a firewall rule`.

![img](/img/how-to/add-new-firewall-rule.png)

3. Add a rule to allow connections from COZYROC Cloud's IP `216.249.109.3` and click `Save` to apply the changes. The rule should look like this:

![img](/img/how-to/firewall-rule.png)


