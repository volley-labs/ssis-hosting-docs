---
title: How to Get Support?
sidebar_label: How to Get Support?
---

To get support for COZYROC Cloud, you can submit a ticket to the [COZYROC Help Center](https://desk.cozyroc.com).

Please note that the Help Center has separate departments for *COZYROC SSIS+* and *COZYROC Cloud*:
* If your question is related to the design/development phase of SSIS package (e.g. [COZYROC SSIS+](/flex/managed/ssis+) installation, task, component or script), you should select the *"SSIS Plus Technical Support"* department.
* If your question or feedback is related to the COZYROC Cloud (its features, web interface, hosted executions, billing, etc.), you should select the *"COZYROC Cloud Support"* department.

Quick links:
* [Submit a COZYROC SSIS+ ticket](https://desk.cozyroc.com/portal/newticket?departmentId=349826000000006907) or browse [COZYROC SSIS+ Knowledge Base](https://desk.cozyroc.com/portal/en/kb/ssis-technical-support)
* [Submit a COZYROC Cloud ticket](https://desk.cozyroc.com/portal/newticket?departmentId=349826000020908523) or browse [COZYROC Cloud Knowledge Base](https://desk.cozyroc.com/portal/en/kb/cloud-support)