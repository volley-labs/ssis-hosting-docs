---
title: Scheduled Jobs
sidebar_label: Scheduled Jobs
---

import YouTubePlayer from "../../src/components/YouTubePlayer/YouTubePlayer.jsx";

:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

To set up an execution (either recurring or one-time) you need to create a scheduled job. You can do this immediately after:
* uploading a package (via the "Add Trigger" button and then by selecting the "Scheduled Job" option) or later - either from Packages page or from Scheduled Jobs page.
* installing a gem (via the "Add Trigger" button and then by selecting the "Scheduled Job" option) or later - either from Gems page or from Scheduled Jobs page.
![img](/img/add-trigger.png)

When configuring recurring jobs you have a lot of flexibility when to schedule them. Basically all options available in SQL Server Agent Schedules are also available in COZYROC Cloud Scheduled jobs:
* Configure time zone
* Configure frequency (daily, weekly or monthly)
* Configure daily frequency (at what time a day or on each what interval)
* Configure the activity period of the scheduled job (from-to date). 

Scheduled jobs can be enabled or disabled (i.e. active or inactive). A one-time job becomes automatically inactive after it gets executed.

## Scheduled Jobs Management

The <a href={process.env.REACT_APP_MANAGEMENT_URL + "/scheduled-job/overview"} target="_blank">Scheduled Jobs page</a> displays a list of all scheduled jobs (active vs. inactive). You can perform the following operations in this page:
* Sort by job name (Name), package name (Package) or gem name (Gem), agent execution type (Agent), time of last execution (Last executed) and the next time it is scheduled to run (Next Scheduled Time).
* Navigate to a selected scheduled job or the corresponding package
* Filter the grid by package or gem
* Add a new Scheduled job (for an already uploaded package or already installed gem)
* Enable or disable job

## Examples

The examples below demonstrate different scheduling scenarios using **SSIS packages**. All steps shown in the videos apply to **Gems** as well.

**Schedule a SSIS package to execute at a specified time**

<YouTubePlayer embedUrl="https://www.youtube.com/embed/4JySWn0AAYg" />

**Schedule a SSIS package to execute once daily**

<YouTubePlayer embedUrl="https://www.youtube.com/embed/4m1jIaPNk4Q" />

**Schedule a SSIS package for executing on a weekly or monthly basis**

<YouTubePlayer embedUrl="https://www.youtube.com/embed/4kl8aSYVDkQ" />
