---
title: SSH Configurations
sidebar_label: SSH Configurations
---

:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

On the Connectivity page, there is a sub-section called **SSH Configurations**. Use this page to set up SSH configurations.

From the SSH Configurations page, you can perform the following actions:

* Create SSH configuration
* Update SSH configuration
* Delete SSH configuration

:::note
SSH configuration name is unique for your organization

SSH tunnel name is unique for each SSH configuration within your organization
:::

## Usage?

To be able to use SSH configuration you have to use our expression syntax as a parameter value or as an environment variable.

e.g. `${SSH::stage:tunnel1.Host}` or `${SSH::stage:tunnel1.Port}`

You can find below what each part of the expressions above mean:
- The `SSH::` prefix is mandatory for every SSH expression
- `stage` is the name of your SSH configuration
- `tunnel1` is the name of the tunnel which you want to use
- `Host` is the server address
- `Port` is the port number

![img](/img/ssh-configurations/ssh-configuration-details.png)

### Expression as a parameter value

Let's assume that you have a package with ADO.NET connection and you want to set up an inbound webhook or a scheduled job connection string parameter to use SSH connection during package execution.
Your connection string parameter value could be: 

`Server=${SSH::stage:tunnel-1.Host},${SSH::stage:tunnel-1.Port};Database=Autobox;User Id=username;Password=password321;`

![img](/img/ssh-configurations/ssh-usage-as-a-parameter.png)

You can use our expression autocomplete editor by checking the `Use expressions` checkbox and then you can type `${` in the input field.
Next you will see a list with suggestions which will navigate you trough the expression configuration.

![img](/img/ssh-configurations/expression-autocomplete-all-suggestions.png)


### As an environment variable

Or you can go even further and make your SSH configuration reusable by creating it as an environment variable as shown below.

![img](/img/ssh-configurations/ssh-create-evn-var.png)

And then use it as an environment variable for your connection string parameter in the execution trigger as shown below.

![img](/img/ssh-configurations/env-var-usage.gif)
