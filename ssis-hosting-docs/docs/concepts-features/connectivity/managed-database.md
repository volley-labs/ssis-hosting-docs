---
title: Managed Database
sidebar_label: Managed Database
---

:::info
Available for Flex (SSIS) only
:::

As part of your `Flex Managed` subscription you can make use of a managed Microsoft SQL Server database to store data during package executions. The database has a maximum size of `500 MB` and can be created for you upon request. To request a database go to `Connectivity -> Managed Database` and click the `REQUEST DATABASE` button.

![img](/img/managed-database/request-database.png)

We will contact you as soon as your database is created. The connection strings to use in your packages will be available on the `Managed Database` page where you made the request.

![img](/img/managed-database/database-connection-strings.png)