---
title: REST Tokens Overview
sidebar_label: Overview
---
:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

On the Connectivity page, there is a sub-section called **REST Tokens**. Use this page to upload or to create a new REST token which you can use later for your executions. 

From the REST Tokens Overview page, you can perform the following actions:

- [Upload](/concepts-features/connectivity/rest-tokens/upload) REST Token file 
  - Those REST tokens are generated during design-time when you configure a corresponding COZYROC SSIS+ [REST Connection Manager](http://www.cozyroc.com/ssis/rest-connection).
- [Create](/concepts-features/connectivity/rest-tokens/create) new REST Token
- Delete REST Token

![img](/img/rest-tokens/rest-tokens-overview.png)

:::info
Once a Rest Token has been created or uploaded, the corresponding REST Connection Manager `TokenFile` property needs to be configured to refer to the REST Token file. This can be done by opening the package, the gem, the scheduled job or the inbound webhook in the COZYROC Cloud app and editing the *TokenFile* parameter of the corresponding Connection Manager to select the corresponding REST Token file that was uploaded.

Since SSIS+ 2.1 onwards the property is called `TokenStore`.