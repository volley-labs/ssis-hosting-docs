---
title: Upload REST Token
sidebar_label: Upload
---
:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

Use this page to upload any REST token files that were created for your executions. 

For Flex (SSIS) those REST tokens are generated during design-time when you configure a corresponding COZYROC SSIS+ [REST Connection Manager](http://www.cozyroc.com/ssis/rest-connection).

Click on the `Browse` button and select a REST token file to upload.

![img](/img/rest-tokens/rest-tokens-upload.png)

Enter REST token name and click `Save`. On success you will be redirected to the details page.

![img](/img/rest-tokens/rest-tokens-upload-success.png)

On that page you can update, download and delete the REST token file.