---
title: Create REST Token
sidebar_label: Create
---

Select configuration, fill the other required fields and click on `Continue` button.

![img](/img/rest-tokens/rest-tokens-create.png)

Next, you will be asked to sign in with your credentials.

![img](/img/rest-tokens/rest-tokens-sign-in.png)

Next, you will be asked to confirm the scopes that COZYROC Cloud will have access to.

![img](/img/rest-tokens/rest-tokens-confirm-scopes.png)

On success, you will be redirected to the details page.

![img](/img/rest-tokens/rest-tokens-create-success.png)

On that page, you can delete or update the REST token.