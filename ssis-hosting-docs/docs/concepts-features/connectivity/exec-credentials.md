---
title: Execution Credentials
sidebar_label: Execution Credentials
---

:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

On the Connectivity page, there is a sub-section called **Execution Credentials**. Use this page to set up Execution Credentials.

From the Execution Credentials page, you can perform the following actions:

* Create credentials
* Delete credentials

Execution credentials feature lets you configure session credentials in the Windows execution environment. This allows seamless access to external services such as SMB file shares.

## Example: Connect to Azure File Share

The following example assumes that you already have an Azure Storage Account with a File Share in it.

1. Open your File Share in Azure.

![img](/img/exec-credentials/open-file-share.png)

2. Click `Connect` to open connection options.

![img](/img/exec-credentials/click-connect.png)

3. Select `Windows` and `Storage account key` as authentication method then click `Show Script`.

![img](/img/exec-credentials/connection-options.png)

4. From the script extract the following: `account URL`, `domain and user`, `storage account key`.

![img](/img/exec-credentials/connect-script.png)

5. Open Cozyroc Cloud, navigate to `Execution Credentials` page and click the `+` button.

![img](/img/exec-credentials/open-exec-credentials.png)

6. Fill the form with the data from the Azure script and click `Save`

![img](/img/exec-credentials/create-exec-credential.png)

7. The execution process can access files from the Azure File Share by their absolute (UNC) path e.g. `\\my-account.file.core.windows.net\my-share\data.csv` while running