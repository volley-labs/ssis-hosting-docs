---
title: Signature Verification
sidebar_label: Signature Verification
---

:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

Some services sign their webhook events to enable verifying that they are the originator of these requests.

If you want to verify webhook requests in COZYROC Cloud, you have to specify in the Webhook details page the signature verification `header` name and the signature verification `secret`, provided by the corresponding third-party service, as shown below:

![img](/img/inbound-webhooks/signature-verification.png)

:::note
Here are few points that you have to be aware of:

- The algorithm that we use to verify the request signature is `HMAC-SHA256`
- Currently only the request body is supported as part of the signature

The hash value is computed by applying the `HMAC-SHA256` algorithm on the request body, along with the verification `secret` that was provided in the webhook configuration.
Then we check if the computed hash matches the hash that was provided in the verification `header`.
:::

#### Example:

If the secret is `SuperSecret007!` and the request body is 
```
{"webhook_id":"7689a169-a000-4985-8676-6902b96d6627","event":"taskCreated","task_id":"c0j"}
``` 
Then the computed signature should be `1f01c7249f0c173e0a77ef78a7cf75ab0ecb71434e6fabf1ada5814029506a79`.