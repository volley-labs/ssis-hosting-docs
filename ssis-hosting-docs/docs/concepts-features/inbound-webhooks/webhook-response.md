---
title: Webhook Response
sidebar_label: Webhook Response
---

:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

In most integration scenarios only the response code will matter. However some external systems have a verification phase (e.g. Sharepoint) as an additional security layer before allowing a webhook integration. You can pass such a phase by configuring the shape of the webhook response body based on the data from the request.

## Content type

The response header will be set accordingly. The selected content type will also determine how the body template is interpreted.

* `application/json` - All response template expressions will be evaluated to the JSON representation of the referenced data. 
* `text/plain` - Response template expressions referencing primitive values will be evaluated as text, nested structures and lists will be discarded.

## Response Template

Configure the response body using our simple template syntax. You can construct a response which will get data from the request `query string`, `headers`, `body` or any combination of them.

* `${Webhook::Query}`
* `${Webhook::Headers}`
* `${Webhook::Body}`

For example given the `request data`

```
// Request Url
https://app.cozyroc.cloud/webhook/v28gX9/904E393C7D6080954B54AF5D1CCD8A81632CDE01739B2409DE10D6A7499BB22E?validationToken=4B54AF5D1CCD8A81632CDE01739

// Request Headers
Authorization: auth 30f9653d1d15460a9cc58d082b7981bf

// Request Body
{
	"OrderId" : 145,
	"Amount" : 5,
	"Customer": {
		"Id" : 148,
		"Name": "John Smith"
	}
}
```

and `response template`
```
{
    "validationToken": ${Webhook::Query.validationToken},
    "authorization": ${Webhook::Headers.Authorization},
    "amount": ${Webhook::Body.Amount},
    "customer": ${Webhook::Body.Customer}
}
```

, the `response body` for `application/json` will be
```
{
    "validationToken": "4B54AF5D1CCD8A81632CDE01739",
    "authorization": "auth 30f9653d1d15460a9cc58d082b7981bf",
	"amount": 5,
    "customer": { 
		"Id" : 148,
		"Name": "John Smith"
	}
}
```

while for `text/plain` it will be
```
{
    "validationToken": 4B54AF5D1CCD8A81632CDE01739, // Text value no double quotes
    "authorization": auth 30f9653d1d15460a9cc58d082b7981bf, // Text value no double quotes
	"amount": 5,
    "customer": // Complex object discarded
}
```

## Example
In this section we are going to configure a webhook to pass the **SharePoint** verification phase.

According to the [SharePoint documentation](https://docs.microsoft.com/en-us/sharepoint/dev/apis/webhooks/overview-sharepoint-webhooks) when a new subscription is created, SharePoint will send a request to the registered URL similar to:

```
POST https://app.cozyroc.cloud/webhook/v28gX9/904E393C7D6080954B54AF5D1CCD8A81632CDE01739B2409DE10D6A7499BB22E?validationtoken={randomString}
Content-Length: 0
```

For the subscription to be created successfully, your webhook must respond with the `validationtoken` query string parameter as a plain-text response:

```
HTTP/1.1 200 OK
Content-Type: text/plain
{randomString}
```


1. Add a webhook trigger for a package or a gem.

2. In the response section of the webhook page select content type `text/plain`.

3. Set the response to

```
 ${Webhook::Query.validationtoken}
```

Your configuration should look like this
![img](/img/inbound-webhooks/webhook-response-template.png)

4. Save the webhook and configure the integration in **SharePoint**.


## Execution status

After triggering an inbound webhook you'll receive a `X-RunId` header in response headers, which will have the id of the execution as a value.
Use `X-RunId` value to construct a `GET` request to check the execution status by appending it to the webhook url like so:

```
GET https://app.cozyroc.cloud/webhook/v28gX9/904E393C7D6080954B54AF5D1CCD8A81632CDE01739B2409DE10D6A7499BB22E/{X-RunId}
```

Then, as a response you will receive the following JSON:

```
{
	"status": "Success"
}
```