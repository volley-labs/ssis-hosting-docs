---
title: Inbound Webhooks Overview
sidebar_label: Overview
---

:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

The inbound webhooks allow events in **external systems** to trigger executions, enabling you to implement **event driven workflows** in COZYROC Cloud. To configure an execution via an inbound webhook you need to create one either from:

 * the package page (via the "Add Trigger" button and then by selecting the "Webhook" option)
 * the installed gem page (via the "Add Trigger" button and then by selecting the "Webhook" option)
 * from the "Webhooks" page

![img](/img/add-trigger.png)

After creation an inbound webhook is enabled by default. If you want to prevent a webhook from being triggered, you can disable it at any time from its page or from the page of its corresponding package or gem.

## Inbound Webhooks Management

The <a href={process.env.REACT_APP_MANAGEMENT_URL + "/webhooks/inbound/overview"} target="_blank">Inbound Webhooks page</a> displays a list of all inbound webhooks (enabled and disabled) and allows you to perform a number of actions:
* Add a new webhook or delete an existing one
* Copy webhook url
* Enable or disable webhooks

## Trigger an execution via Webhook

You can trigger a webhook by copying the webhook URL from its details page and then making a `HTTP GET` or `HTTP POST` request to that URL.

![img](/img/inbound-webhooks/copy-webhook-url.png)

* `HTTP GET` - verification only, doesn't trigger execution, supports reading data from request's **query string** and **headers**
* `HTTP POST` - supports reading data from request's **query string**, **headers** and **body**

## Using external event data in your package or gem

The webhook page allows to set parameters based on the event data sent with the webhook request. Currently we support 3 types of data sources:
* `Body` - gets data from request body (only for `HTTP POST` requests) as text
* `HTTP Headers` - gets data from request headers and serializes it to JSON in a key-value format
* `Query` - gets data from query string and serializes it to JSON in a key-value format

![img](/img/inbound-webhooks/assign-webhook-data.png)

We support nested objects. Hence it is possible your expression to specify source property.

e.g. `${Webhook::Query.validationToken}` or `${Webhook::Headers.Authorization}` or `${Webhook::Body.id}`

Learn more about the topic [here](/concepts-features/inbound-webhooks/call-webhook-with-request-data).

## Response configuration

Some external systems will require a response from the webhook (e.g. as part of a verification phase). From the webhook page you are able to configure the **response** details - choose a `content type` and use a simple template syntax to build the **response body**. All `event data sources` are also available here (Body, Headers and Query). 

![img](/img/inbound-webhooks/custom-response.png)

Learn more about the topic [here](/concepts-features/inbound-webhooks/webhook-response).

## Notifications

Similar to Scheduled jobs you can set the Webhook to send email notifications to users upon execution failure or success. 