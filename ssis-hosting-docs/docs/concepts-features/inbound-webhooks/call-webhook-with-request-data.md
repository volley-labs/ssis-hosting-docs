---
title: Call webhook with request data
sidebar_label: Webhook w/ Request Data
---

:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

## Data Sources

Any parameter of type `String` can be configured to get its value from the webhook request data. There are 3 request data sources:

### Body

Expression `${Webhook::Body}` will be evaluated as the whole request body converted to a string.

The whole request body will be converted to string and set as parameter value. A body with one of the following content types: `application/json`, `application/xml` and `text/plain` will be applied directly as it is received. In case a different content type is detected, it will be converted to Base64 encoded string and used this way. 

For `application/json` request bodies we support extracting of specific properties. Given the following body

```
{
    "order_id": "xxx-1234",
    "customer_id": "ccc-56"
}
```

you can get the `order_id` value directly with expression `${Webhook::Body.order_id}`. It will be evaluated as `xxx-1234`.

:::note
* The request body size should be **less than or equal to 2 MB** (exceeding the limit will result in **413 Request Entity Too Large** response).
* The request should be **HTTP POST**.
:::

### Headers

With expression `${Webhook::Headers}` all request headers will be collected and converted to JSON key-value format.

For example request headers

```
Host: cozyroc.com
Origin: https://cozyroc.com
Referer: https://cozyroc.com/
```

will be converted to

```
{
    "Host": ["cozyroc.com"],
    "Origin": ["https://cozyroc.com"],
    "Referer": ["https://cozyroc.com/"]
}
```

To extract a specific header value use the following syntax `${Webhook::Headers.[header key]}`. Using the example data above `${Webhook::Headers.Host}` will be evaluated as `cozyroc.com`.

### Query String

With expression `${Webhook::Query}` the request query string will be converted to JSON key-value format.

For example request query string

```
?user=john&code=68a0711871ab41e89f35d6e6e3a9f8b1
```

will be converted to

```
{
    "user": ["john"],
    "code": ["68a0711871ab41e89f35d6e6e3a9f8b1"]
}
```

To extract a specific query parameter value use the following syntax `${Webhook::Query.[parameter name]}`. Using the example data above `${Webhook::Query.user}` will be evaluated as `john`.

## Parameterization

**Prerequisites:**

* **For Gems**: select and install a gem from the Gem gallery
* **For Flex**: while developing a package create one or more parameters that will hold the webhook request data and upload your package 
![img](/img/inbound-webhooks/package-webhook-param-body.png)

1. From the Gem or Package page add a webhook trigger.

![img](/img/add-trigger.png)

2. In the **Parameters** section of the webhook page click on the ![img](/img/configure_icon.png)(Gems) icon or the ![img](/img/edit_icon.png)(Flex) icon next to a parameter.

3. Configure the webhook data source for each of your parameters. 

![img](/img/inbound-webhooks/parameterize-webhook.png)

4. After the configuration is done, your parameter value will be displayed like e.g. `${Webhook::Body}`

5. Click **Create** to save your webhook configuration.


## Testing Webhook

1. Copy Url from the webhook page.

![img](/img/inbound-webhooks/copy-webhook-url.png)

2. Open your favorite API client (e.g. Postman, Insomnia or Visual Studio Code with Thunder Client extension). In this guide we are going to use **Postman**.

3. Choose the HTTP verb. We currently support `HTTP GET` and `HTTP POST` for all webhook endpoints. **HTTP GET** should be used only for verification purposes and **won't** trigger an execution. Also note that request body is available only for **HTTP POST** requests. 

4. Edit your request data.

![img](/img/inbound-webhooks/override-webhook-param.png)

5. If the request is successful you should receive `200 OK` as a response. If the webhook is disabled  you will receive `403 Forbidden` as a response.

6. After the execution is completed you can verify the execution parameters on execution details page.

![execution-result](/img/inbound-webhooks/execution-result.png)

## Call History

The _Call History_ tab contains the list of all HTTP calls to the current inbound webhook. The time when the HTTP call event has occured (_Timespamp_), the request HTTP method (_Method_) and the response status code (_Response_) are directly available in the table. If you click on the link item dispayed in the first column a dialog will appear with all the details related to the call.

![execution-result](/img/inbound-webhooks/webhook-history.png)

**Webhook Log Details** contains the following sections:
- _Webhook Name_
- _Timestamp_
- _Method_
- _Status Code_
- _Query (Request querystring)_
- _Headers (Request headers)_
- _Body (Request body)_
- _Response Content_
- _Response Type_

![execution-result](/img/inbound-webhooks/webhook-history-log.png)




