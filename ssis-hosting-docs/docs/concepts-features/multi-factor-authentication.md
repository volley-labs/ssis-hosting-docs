---
title: Multi-factor Authentication
sidebar_label: Multi-factor Authentication
---

We strongly recommend enabling multi-factor authentication (MFA) to increase the security of your account. COZYROC Cloud supports MFA via **Google Authenticator**. Google Authenticator is a mobile TOTP app and supports secure backup of your authentication codes in the cloud and so they can be restored if you lose access to your device. To configure MFA follow the steps below:

1. Install **Google Authenticator** on your mobile device.

2. Open COZYROC Cloud and navigate to your **Profile** page.

![img](/img/mfa/profile.png)

3. Expand the **Multi-factor authentication** section and click **Enable MFA**.

![img](/img/mfa/mfa.png)

4. Scan the **QR code** or enter the **setup code** as a `time-based` code manually into Google Authenticator.

![img](/img/mfa/setup.png)

5. Enter the Google Authenticator 6-digit PIN code into the dialog to verify and complete the setup.