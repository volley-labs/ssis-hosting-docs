---
title: User Management
sidebar_label: User Management
---

import YouTubePlayer from "../../src/components/YouTubePlayer/YouTubePlayer.jsx";

COZYROC Cloud makes it easy to collaborate with your teammates on SSIS executions. An account administrator can invite users and assign them roles.

## Inviting Users

Users can be invited to an account by specifying their e-mail and choosing one or more roles. After inviting a user, they will be sent an e-mail. The e-mail can be resent in case of a problem. An invitation can be revoked.

<YouTubePlayer embedUrl="https://www.youtube.com/embed/VQVde6_ypUw" />

:::note
A user is identified by their e-mail and a user can belong to a single account. If a user needs to have access to multiple accounts (i.e. multiple COZYROC Cloud subscriptions), they'll need to use a distinct e-mail for each account.
:::

## User Roles

There are 4 predefined roles and a user can belong to several of them:
* **Viewer** - Granted VIEW permissions related to packages / scheduled jobs / inbound webhooks / executions within the account
* **Operator** - Granted ALL permissions related to packages / scheduled jobs / inbound webhooks / executions within the account
* **Administrator** - Granted ALL permissions within the account
* **Billing** - Granted ALL permissions related to the billing of the account

## Suspending Users

To suspend/unsuspend a user, go to the <a href={process.env.REACT_APP_MANAGEMENT_URL + "/users/user/overview"} target="_blank">list of users</a>, select a user and click "Suspend" or "Unsuspend".

## Locking Users

To prevent brute-force attacks on user passwords, a user account can be automatically locked for a certain period of time (e.g. 5 minutes) if there are 5 failed password guessing attempts.

## Audit Log

Administrator users have access to the Audit log. It includes all user activity that affects entities/artifacts that are accessible to multiple users in an account (i.e. packages, scheduled jobs, inbound webhooks, executions, etc). Non-admin users are allowed to view only their own activity.