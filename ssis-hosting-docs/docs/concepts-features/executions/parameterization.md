---
title: Execution Parameterization
sidebar_label: Parameterization
---
## Gems (Pre-built)

Any Gem you install from the COZYROC Cloud Gallery has a pre-defined list parameters allowing you to customize the behavior according to your needs. The Gem `Details` tab contains detailed descriptions for all parameters and how to use them.

### Gem Parameters

Gem parameters can be found in the `Parameters` tab on every page that allows parameterization. They are usually grouped by use case for convenience.

![img](/img/gems/gem-parameters.png)

### How to parameterize values?

You can edit parameter values directly or click on the ![img](/img/configure_icon.png) icon next to a prameter to open the **Edit** dialog. There you can specify an expression or choose *"use environment"* and select a variable from an environment linked to a trigger. See more info about [environments](/concepts-features/environments). See more info about expressions: [environment expressions](/concepts-features/environments#environment-variables-usage), [webhook expressions](/concepts-features/inbound-webhooks/call-webhook-with-request-data), [SSH expressions](/concepts-features/connectivity/ssh-configurations#usage).

## Flex (SSIS)

Any SSIS package you upload on the COZYROC Cloud can be a template for multiple scheduled jobs or inbound webhooks, as it can be further parameterized. For example, suppose you need to use different connection strings for your staging and production environments. Instead of uploading two very similar versions of a package, you only need to parameterize a package in two different ways.

### Package Parameters

Package parameters allow you to easily modify package execution without having to edit and redeploy the package. The parameters must be set up when the package is designed. Then, when the package is uploaded to the COZYROC Cloud, the parameters will show up under “Parameters” when the package is opened.

#### Parameter predefined values

You can configure a list of allowed values for a package parameter for `Numeric` and `String` parameters. To achieve that you have to go to package details and pick a parameter to edit.
Then you click on `Settings` tab and check `Add predefined values` checkbox. Add wanted values and click `Apply`.

![img](/img/parameter-editor/add-predefined-values.png)

:::caution
In case you have added parameter predefined values then your parameter value must be one of these values.
:::

Hence, when you create or update a `scheduled job` or `inbound webhook` you will be allowed to select a parameter value only from the predefined list.

![img](/img/parameter-editor/trigger-predefined-values-ddl.png)

### Connection Manager Properties

All connection manager properties can be modified once a package has been uploaded to the COZYROC Cloud. The properties will initially have the values configured at design time. In addition, if no password is specified when the package is uploaded, then any sensitive information will need to be entered manually in the connection managers' properties (in the package, in the scheduled job(s) or in the inbound webhook(s)).

### Property Overrides
Even if you haven't introduced a package parameter for easy customization of a particular property, you will still be able to modify any property value via a property override. You just need to specify the full path of the property.

### How to parameterize values?

Click on the ![img](/img/edit_icon.png) icon next to a prameter to open the **Edit** dialog. There you can either specify a value or an expression directly or choose *"use environment"* and select a variable from an environment linked to a trigger. See more info about [environments](/concepts-features/environments). See more info about expressions: [environment expressions](/concepts-features/environments#environment-variables-usage), [webhook expressions](/concepts-features/inbound-webhooks/call-webhook-with-request-data), [SSH expressions](/concepts-features/connectivity/ssh-configurations#usage).

**NOTE:** The `TokenFile` property of the COZYROC SSIS+ [REST Connection Manager](http://www.cozyroc.com/ssis/rest-connection) requires a custom setup (see [here](/concepts-features/connectivity/rest-tokens/overview)). A dropdown with a list of tokens appears when you parameterize this property.

### Customize UI

Configuring an execution trigger usually involves setting specific values for different parameters and connection manager properties. This task may become cumbersome in case you have to work with multiple packages with a lot of parameters. After a package is uploaded COZYROC Cloud allows you to organize parameters and connection manager properties in `UI Groups` and to give them meaningful display names and descriptions.

UI groups are created and managed in the `Customize UI` tab inside the `Parameter` section of a package page. 

![img](/img/parameter-editor/customize-ui.png)

You can change the display order of your groups by dragging and dropping using the handle in the top left corner of each group.

![img](/img/parameter-editor/group-customization.png)

Parameters and connection manager properties can be added to a group in two ways:
* Click on `Add parameters` at the bottom of a group and use the dialog to select one or more parameters.
![img](/img/parameter-editor/add-group-parameter.png)
* Use the `Change UI group` action for a specific parameter directly from `Parameters` or `Connect managers` tab.  
![img](/img/parameter-editor/add-parameter-to-group.png)

Once a parameter is added to a group you have a number of options:
1. Change the parameter display order by dragging and dropping using its handle.
2. Edit the parameter default value.
3. Move parameter to another group.
4. Change the parameter display name.

![img](/img/parameter-editor/parameter-customization.png)

If at least one UI group is created for a package, the default parameters visualization in triggers and executions for this package will automatically change to structured view with a separate section for each UI group.

![img](/img/parameter-editor/simple-view.png)

You can go back and forth between the initial view and the new customized view by toggling the `Switch to Advanced View` switch on and off.

![img](/img/parameter-editor/switch-to-advanced.png)
