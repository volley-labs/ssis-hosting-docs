---
title: Executions Overview
sidebar_label: Executions Overview
---

:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

## Execution types

* **[Scheduled](/concepts-features/scheduled-jobs)** - recurring executions or one-time execution at a specific time
* **[Inbound Webhook](/concepts-features/inbound-webhooks/overview)** - executions triggered by external services via HTTP request
* **[Post-execute Action](/concepts-features/post-execute-actions)** - executions triggered on success of another execution
* **On-demand** - executions triggered manually, useful for one-time runs and testing parameterization

## Execution Statuses

At any given time, an execution can be in one of the following statuses:

* **Pending** - is preparing for execution
* **Running** - is currently being executed by the runner
* **Succeeded** - has completed normally
* **Failed** - has completed with failure
* **Timed Out** - didn't complete within the maximum allowed time (by default 30 minutes)
* **Aborted** - internal error (e.g. hasn't started or hasn't completed normally)

## General Tips

### Working with temporary files

:::info
Available for Flex (SSIS) only
:::

In general, the sources and destinations used in your packages should be cloud-based or accessible via the Internet. You will have access to 1GB of temporary storage space on the COZYROC Cloud execution server, so it should be used sparingly. There is no guarantee that files you write on the server will be available for a prolonged period of time. Any files of interest that are created during package execution should be moved off the server ASAP and that's best done from within the package that generated them. It may also be possible to access them and move them using another package, as long as that package is run soon after the package that created them completes.

## Further Reading
* How to [parameterize executions](/concepts-features/executions/parameterization)
* How to set up [e-mail notifications](/concepts-features/executions/notifications)
* How to configure and analyze [execution logs](/concepts-features/executions/logs)
