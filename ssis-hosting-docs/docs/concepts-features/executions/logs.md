---
title: Execution Logs
sidebar_label: Execution Logs
---

:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

Once an execution is started you can monitor its progress in real time. The logs for completed executions are accessible for 14 days. You can also download the log file in JSON format.

### Logging Levels

* **None** - no log entries from the SSIS runtime will be recorded (only system logs)
* **Basic** - basic SSIS logging (that's the recommended logging level)
* **Verbose** - verbose SSIS logs (**NOTE:** Use only for troubleshooting purposes)
* **Profiled** - Flex (SSIS) only. Provides verbose SSIS logs and enables [execution profiling](/concepts-features/executions/profiling) (**NOTE:** Use only for troubleshooting purposes)
