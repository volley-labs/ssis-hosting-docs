---
title: Execution Notifications
sidebar_label: Execution Notifications
---

import YouTubePlayer from "../../../src/components/YouTubePlayer/YouTubePlayer.jsx";

:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

You can receive e-mail notifications about the result of executions. By default, you will be notified only for failed executions, but you can enable notifications for successful executions as well. Even if you are not set up to receive e-mail notifications, you may check the Executions page to see the status of any scheduled job or inbound webhook execution.

There are 3 ways to control what e-mail notifications to receive:
* In a scheduled job or in an inbound webhook, you can toggle notifications for successful/failed executions (affects all users in the account). 
* In a scheduled job or in an inbound webhook, you can toggle personal notifications for successful/failed executions (affects only you).
* In your profile, you can globally disable all e-mail notifications (affects only you).

<YouTubePlayer embedUrl="https://www.youtube.com/embed/ES4gRxDPfLg" />