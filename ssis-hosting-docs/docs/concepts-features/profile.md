---
title: Manage Your Profile
sidebar_label: Profile Management
---

import YouTubePlayer from "../../src/components/YouTubePlayer/YouTubePlayer.jsx";

In your <a href={process.env.REACT_APP_MANAGEMENT_URL + "/profile"} target="_blank">profile page</a> you can edit your personal information and your preferences:
* First and last name
* Timezone
* Password
* Notifications preferences (more details [here](/concepts-features/executions/notifications))
* Multi-factor authentication

<YouTubePlayer embedUrl="https://www.youtube.com/embed/4n6Tbreri9E" />

:::caution
You can also delete entirely your profile data from the "Danger zone" section. Please note, that you can cancel your subscription without deleting your data!
:::
