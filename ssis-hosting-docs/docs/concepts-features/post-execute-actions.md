---
title: Post-execute Actions
sidebar_label: Post-execute Actions
---

:::info
Available for Flex (SSIS) and Gems (Pre-built)
:::

A post-execute action is a special kind of execution trigger that is executed on `success` of another trigger. Post-execute actions can be configured for any other scheduled job, inbound webhook or post-execute action and allow to execute a sequence of packages or gems (`chain`). To create an action open any other trigger, click on `More` from the bottom toolbar and select `Add post-execute`.

![img](/img/post-execute-actions/add-post-execute.png)

## Configuration

Common trigger configuration is available for post-execute actions as well. Select a package or a gem, runtime and log level in the `Details` panel, configure parameters in the `Parameters` panel and set up execution notifications in the `Notifications` panel.

## Navigation

When the currently opened trigger is part of a `chain` some additional controls will be displayed to allow easier navigation between the steps in this chain. Using the `Trigger chain` menu just below the page title you can quickly navigate between different steps in the `chain`. 

![img](/img/post-execute-actions/trigger-chain-navigation.png)

A more detailed overview of the sequence of packages or gems to be executed is available in the `Trigger Chain` panel at the bottom of the page.

![img](/img/post-execute-actions/trigger-chain-panel.png)

Additionally `executions` produced by the same trigger chain are linked together. On every chained execution page there is a link to the `previous execution` in the chain.

![img](/img/post-execute-actions/chained-execution.png)

## Execution flow

### Default flow

Normally trigger chains will be executed in sequence until the end of the chain is reached or one of the execution steps fails. Example:

* Scheduled job - Step 1
* Post-execute - Step 2
* Post-execute - Step 3
* Post-execute - Step 4

When `Step 1` is triggered, the chain will produce 4 sequential executions: Step 1 -> Step 2 -> Step 3 -> Step 4.

### Disabled steps

In a trigger chain `disabled` steps will be `skipped` and the execution will jump to the next `enabled` step. Example:

* Scheduled job - Step 1
* Post-execute - Step 2
* Post-execute - Step 3 (disabled)
* Post-execute - Step 4

When `Step 1` is triggered, the chain will produce 3 sequential executions skipping `Step 3`: Step 1 -> Step 2 -> Step 4.

### Deleted steps

When a chain step is `deleted`, the system will automatically "repair" the chain by connecting the deleted step's previous and next steps. Example:

* Scheduled job - Step 1
* Post-execute - Step 2
* Post-execute - Step 3 (deleted)
* Post-execute - Step 4

When `Step 1` is triggered, the chain will produce 3 sequential executions since `Step 3` is no longer part of the chain: Step 1 -> Step 2 -> Step 4.

### Run a step manually

When a step is triggered manually by clicking the `Run now` button, the chain will be executed from that point forward. Example:

* Scheduled job - Step 1
* Post-execute - Step 2
* Post-execute - Step 3 (triggered manually)
* Post-execute - Step 4

Manually running `Step 3` will produce 2 sequential executions: Step 3 -> Step 4.

### Retry failed step

When a step's execution fails, the chain will stop. If the failed execution is retried by using the `Retry` button on the Execution page and this time it succeeds, the chain will continue from that point forward. Example:

* Scheduled job - Step 1 (successful execution)
* Post-execute - Step 2 (failed execution)
* Post-execute - Step 3
* Post-execute - Step 4

Retrying `Step 2` will produce 3 sequential executions: Step 2 -> Step 3 -> Step 4.