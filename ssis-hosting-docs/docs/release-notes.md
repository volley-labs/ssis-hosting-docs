---
title: Release Notes
sidebar_label: Release Notes
---
## March 2025
* [COZYROC Cloud Partner Program](https://cozyroc.cloud/partnership)

## October 2024
* Introduced [multi-factor authentication](/concepts-features/multi-factor-authentication)
* UX improvements

## September 2024

* [Managed SQL Database](/concepts-features/connectivity/managed-database) feature for organizations in Flex Managed plan
* On-premises Agent now reports connectivity status in its control panel
* Fixes and improvements

## August 2024

* New [execution profiling](/concepts-features/executions/profiling) feature
* Various optimizations and improvements

## April 2024

* Introduced new major feature: [Gems](/gems/overview)
* SSIS feature is now called [Flex](/flex/overview)
* Fixes and improvements

## October 2023

* [Post-execute actions](/concepts-features/post-execute-actions)
* Various small fixes and improvements

## September 2023

* Manual retry feature for failed executions
* Execute package directly from the Package details page
* Signature verification for inbound webhooks
* UX enhancements

## March 2023

* On-premises Agent [control panel](/flex/on-premises/user-interface)
* Allow to configure a [list of allowed values](/concepts-features/executions/parameterization#parameter-predefined-values) for a package parameter
* UX enhancements

## December 2022

* Security updates and fixes
* Improvements in the Package upgrade workflow
* UX enhancements

## November 2022

* On-premises agent self-update [since version `2.1.1.7609`](/flex/on-premises/agent#agent-self-update-recommended)
* Inbound webhook [call history](/concepts-features/inbound-webhooks/call-webhook-with-request-data#call-history)
* Various small fixes and improvements

## September 2022

* Execution credentials management
* Real-time status updates in Execution overview

## August 2022

* SSH configurations
* Package upgrade assistance
* Scheduling execution per minute intervals
* Autocomplete expression editor for all kind of expressions: webhooks, environment variables, SSH
* Upgrade managed runtime installation to latest SSIS+ 2.1 build

## May 2022

* The application is no longer "Beta":
* Inbound webhooks
* Various small fixes and improvements (app and documentation)

## February 2022

* On-premises executions (via an on-premises agent)
* Upgrade managed runtime installation to SSIS+ 2.1
* Various small fixes and improvements (app and documentation)

## July 2021

* More robust communication between services via RabbitMQ
* Various small fixes and improvements

## May 2021

* Various small fixes and improvements

## March 2021

* Improvements in billing and subscription management
* Upgrade managed runtime to latest SSIS+
* Various small fixes and improvements

## February 2021

* Dashboard and Execution overview improvements
* Various small fixes and improvements

## January 2021

* Initial "Beta" release