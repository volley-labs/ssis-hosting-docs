// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'COZYROC Cloud Docs',
  tagline: 'The Simplest Way to SSIS in the Cloud',
  url: process.env.REACT_APP_DOCS_URL ?? "",
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  noIndex: process.env.REACT_APP_ENV !== 'production',
  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'volley-labs', // Usually your GitHub org/user name.
  projectName: 'ssis-hosting-docs', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },
  themes: ['docusaurus-theme-search-typesense'],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      colorMode: {
        defaultMode: 'light',
        disableSwitch: true,
      },
      footer: {
        style: 'light',
        copyright: 'c'
      },
      typesense: {
        typesenseCollectionName: process.env.REACT_APP_TYPESENSE_COLLECTION,

        typesenseServerConfig: {
          nodes: [
            {
              host: process.env.REACT_APP_TYPESENSE_HOST,
              port: process.env.REACT_APP_TYPESENSE_PORT,
              protocol: process.env.REACT_APP_TYPESENSE_PROTOCOL,
            }
          ],
          apiKey: process.env.REACT_APP_TYPESENSE_API_KEY,
        },
      },
    }),

  presets: [
    [
      '@docusaurus/preset-classic',
      ({
        docs: {
          routeBasePath: "/",
          sidebarPath: require.resolve('./sidebars.js'),
        },
        blog: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        gtag: {
          trackingID: 'G-H29NR47VNB',
          anonymizeIP: true
        },
      }),
    ],
  ],

  plugins: [
    [
      "docusaurus-plugin-dotenv",
      {
        systemvars: true,
      },
    ],
  ],
};

module.exports = config;