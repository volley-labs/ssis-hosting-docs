#!/bin/bash

while getopts u:d:b: flag
do
    case "${flag}" in
        u) git_repo_url=${OPTARG};;
        d) git_checkout_dir=${OPTARG};;
        b) git_branch=${OPTARG};;
    esac
done

if [ ! -d "$git_checkout_dir/.git" ]
then
  echo "Cloning repository into $git_checkout_dir."
  git clone "$git_repo_url" "$git_checkout_dir"
fi

trap 'popd' EXIT

pushd "$git_checkout_dir"
echo "Setting git origin url."
git remote set-url origin "$git_repo_url"
echo "Fetching remote changes."
git fetch --quiet
echo "Cleaning untracked files."
git clean -x -d -f

if [[ $(git rev-parse --abbrev-ref HEAD) == "$git_branch" ]]
then
  echo "Already on branch '$git_branch', resetting to remote."
  git reset --hard origin/"$git_branch"
elif [ `git branch --list $git_branch` ]
then
  echo "Checking out local branch '$git_branch'."
  git checkout --quiet "$git_branch"
  echo "Resetting to remote."
  git reset --hard origin/"$git_branch"
else
  echo "Checking out remote branch '$git_branch'."
  git checkout --quiet --track origin/"$git_branch"
fi
