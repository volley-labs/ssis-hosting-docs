#!/bin/bash

while getopts d:e: flag
do
    case "${flag}" in
        d) git_checkout_dir=${OPTARG};;
        e) environment=${OPTARG};;
        *) echo Unknown option: "$OPTARG";;
    esac
done

trap 'popd' EXIT

pushd "$git_checkout_dir/ssis-hosting-docs" || exit

echo "Starting TypeSense scraper for $environment ..."
docker run --rm --env-file="scraper.$environment.env" typesense/docsearch-scraper