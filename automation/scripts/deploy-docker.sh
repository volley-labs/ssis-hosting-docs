#!/bin/bash

while getopts d:e: flag
do
    case "${flag}" in
        d) git_checkout_dir=${OPTARG};;
        e) deploy_environment=${OPTARG};;
        *) echo Unknown option: "$OPTARG";;
    esac
done

trap 'popd' EXIT

image_name="ssis-hosting-docs:latest"
container_name="ssis-hosting-docs"
pushd "$git_checkout_dir/ssis-hosting-docs" || exit

# hot reload configuration if Caddy is already running
echo "Trying to stop and remove existing Docs container"
docker stop $container_name || true && docker rm $container_name || true
echo "Building Docs image."
docker build -t $image_name --build-arg ENVIRONMENT=$deploy_environment .
echo "Starting Docs container."
docker run -d --name $container_name --restart=unless-stopped -p 13000:80 $image_name